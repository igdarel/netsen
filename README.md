## Apprentissage de concepts Backend avec Entity Framework Core

1. Suivre le tutoriel et reproduire l'exemple : https://docs.microsoft.com/en-us/ef/core/get-started/index?tabs=netcore-cli
2. Rajoutez un modèle <<Author>> avec les attributs <<Nom, prenom, photo de profile ... etc >> 
*	Un Auteur/Author est le Owner/titulaire d'un ou de plusieurs Post(s)
* Un Post est fait par un et un seul Author
* Un Post peut avoir un ou plusieurs "Commentaire(s)"
* Un Commentaire est effectué sur un Post et est effectué/émis par un et un seul Auteur/Author
3. Produisez le Modèle Conceptuel de Données (MCD) permettant de représenter cette BD
4. Identifiez toutes les entités requises et Rajoutez ces nouvelles entités dans votre exemple

---

## Apprentissage de concepts Frontend avec Xamarin Forms

1. Lire les cours suivants :
* https://docs.microsoft.com/en-us/xamarin/xamarin-forms/
*	https://dotnet.microsoft.com/apps/xamarin/xamarin-forms
*	https://docs.microsoft.com/en-ca/xamarin/xamarin-forms/app-fundamentals/shell/introduction
* https://docs.microsoft.com/en-us/xamarin/get-started/first-app/?pivots=windows
2. Reproduire cet exemple : https://docs.microsoft.com/en-us/xamarin/get-started/quickstarts/single-page?pivots=windows
3. Modifier l'exemple de Xamarin.Forms en utilisant le concept des Command a la place des Event-Clicked sur les composants. Vous trouverez tous les renseignements au sujet de MVVM et du Command Design Patern dans les cours à lire
4. Rajoutez une fenêtre modale de confirmation lorsque on clique sur "Delete" d’une "Note"
5. Vous devrez "deleter/supprimer" la Note uniquement si l'usager confirme sur la modale. S’il clic sur NON, alors ne rien faire.